package ru.tsc.kitaev.tm.api.repository;

import ru.tsc.kitaev.tm.model.AbstractEntity;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    void remove(E entity);

    void clear();

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    E findById(String id);

    E findByIndex(Integer index);

    E removeById(String id);

    E removeByIndex(Integer index);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    Integer getSize();

}
