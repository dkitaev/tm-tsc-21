package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.model.User;

import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public static Predicate<User> predicateByLogin(final String login) {
        return s -> login.equals(s.getLogin());
    }

    public static Predicate<User> predicateByEmail(final String email) {
        return s -> email.equals(s.getEmail());
    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        return list.stream()
                .filter(predicateByLogin(login))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return list.stream()
                .filter(predicateByEmail(email))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}
