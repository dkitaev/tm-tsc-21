package ru.tsc.kitaev.tm.command.task;

import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskIsUnbindFromProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "tasks-unbind-from-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(userId, projectId) == null) throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findById(userId, taskId) == null) throw new TaskNotFoundException();
        final Task taskUpdated = serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

}
