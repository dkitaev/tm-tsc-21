package ru.tsc.kitaev.tm.exception.user;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }
}
